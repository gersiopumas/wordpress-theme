<?php 
$featured_img_url = get_the_post_thumbnail_url(null,'full');
$thumbnail = esc_url($featured_img_url);
?>  

<div class="flex basis-25 flex-col tour-mobile-item gray-bg">
  <img class="max-img-200" src="<?=$thumbnail?>"/>
  <div class="flex flex-col tour-content j-a-center">
    <p class="flex text-center side-padding small-margin single-tour-title"><strong><?php the_title(); ?></strong></p>
    <p class="flex side-padding j-a-center text-center">
      <?=get_the_excerpt(); ?>
    </p>
    <a 
      href="<?=get_permalink();?>"
      class="flex flex-center border-radius yellow-bg white-text max-h-30 margin-b-10 view-tour-hover " 
    > 
      <?php pll_e('Reservation-Text');?>
    </a>
  </div>
</div>