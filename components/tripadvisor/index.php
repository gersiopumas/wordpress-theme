<div class="flex flex-col j-a-center">
  <img src="<?=get_bloginfo('template_directory')?>/assets/trip-advisor.png"/>
  <p class="ocean-text top-tours-title bold">
    <?php pll_e('Trip-Advisor-View-Comments');?>
  </p>

  
  <div class="comment-slider flex flex-row j-a-center basis-100">
    <div class='flex comment-item j-a-center'>
      <p>“Desde la llegada fuimos recibidos con una gran sonrisa (y gel antibacterial). La información fue clara antes de subir al velero y durante el recorrido. Sin duda regresaremos cuando la laguna recupere sus colores.”</p>
    </div>
    <div class='flex comment-item j-a-center'>
      <p>“We booked a private tour and had an amazing time exploring the lake. Our guides were very knowledgeable and make it a memorable experience for my girlfiend and I. I would highly recommend booking with this tour operator.”</p>
    </div>
    <div class='flex comment-item j-a-center'>
      <p>“Rien de mieux pour avoir une vue de cette splendide lagune. Nous étions seulement six sur notre bateau, par une journée magnifique. Les arrêts vous permettent de profiter de cette eau rafraîchissante. Le dernier arrêt, au passage des pirates, vous permet même de vous nettoyer la peau , grâce aux minéraux inclus dans le sable. Une très belle journée!”</p>
    </div>
    <div class='flex comment-item j-a-center'>
      <p>“Visitamos los sitios más hermosos de la laguna donde no hay multitudes y parece que estamos solos en la laguna, disfrutamos también de los sitios más populares e importantes donde aprendimos de la historia del lugar. El staff muy preparado siempre atento a nuestras necesidades. El mejor servicio que recibimos en nuestra estancia en Bacalar!”</p>
    </div>
    <div class='flex comment-item j-a-center'>
      <p>“Super journée de découverte de la lagune. Stops dans des endroits variés pour profiter d’une eau et d’une lumière fantastiques. Sebastian et Ricardo savent partager leurs connaissances et leur passion de Bacalar. Du coup, leur compagnie participe au plaisir de profiter les paysages. Réclamez-les, ils sont top !!!”</p>
    </div>
  </div>
</div>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
  $('.comment-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: false,
    adaptiveHeight: true,
    nextArrow: '<i class="spli-arrow-right slick-arrow"></i>',
    prevArrow: '<i class="spli-arrow-left slick-arrow"></i>',
  });
</script>
