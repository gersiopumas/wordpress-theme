<section class="flex flex-col j-a-center top-tours-container">
  <p class="flex j-a-center ocean-text top-tours-title">
    <strong><?php pll_e('Section-Top-Tours-Title'); ?></strong>
  </p>
  <p><?php pll_e('Section-Top-Tours-Description'); ?></p>
  <div class="flex flex-row flex-wrap j-a-center">
    <?php
      $tag = "top-tour-".pll_current_language();
      $args=array('posts_per_page'=>5, 'tag' => $tag );
      $wp_query = new WP_Query( $args );
      $count = 0;
      if ( have_posts() ) :
        while (have_posts() && $count<4) : the_post();
          get_template_part( 'template-parts/single-tour');
          $count+=1;
        endwhile;
      endif;
      $wp_query = null;
      $wp_query = $original_query;
      wp_reset_postdata();
    ?>
  </div>
</section>