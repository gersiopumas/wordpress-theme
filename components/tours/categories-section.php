<section class="flex flex-col j-a-center top-tours-container j-a-center width-100">
  <div class="flex flex-row force-flex-wrap side-margin j-a-center width-100">
    <?php
		  if ( have_posts() ) :
			  /* Start the Loop */
			  while ( have_posts() ) :
			  	the_post();

			  	/*
			  	 * Include the Post-Type-specific template for the content.
			  	 * If you want to override this in a child theme, then include a file
			  	 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
			  	 */
			  	get_template_part( 'template-parts/single-tour');

			  endwhile;
			  the_posts_navigation();
		  else :
			  get_template_part( 'template-parts/content', 'none' );
		  endif;
    ?>
    <!-- test -->
</div>
  
</section>