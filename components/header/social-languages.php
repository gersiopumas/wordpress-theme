<div class="hide-md" id="social-contact">
  <a href="https://www.facebook.com/daytourbacalar"><i class="spli-facebook blue-text"></i></a>
  <a href="https://wa.me/5219982315864"><i class="spli-whatsapp blue-text"></i></a>
  <a href="#" class="blue-text">Blog</a>
</div>

<a lang="en-US" hreflang="en-US" href="/en"><img class="lang-small" src="<?=get_bloginfo('template_directory')?>/assets/us_small.png"/></a>
<a lang="es-MX" hreflang="es-MX" href="/"><img class="lang-small" src="<?=get_bloginfo('template_directory')?>/assets/mex_small.png"/></a>
<a href="<?php pll_e('Header-Contact-URL'); ?>" class="border-radius blue-border blue-bg white-text margin-s-10 contact-hover"><?php pll_e('Header-Contact-Us'); ?> </a>
