<nav class="flex navbar">
  <div class="flex j-a-center basis-20 ">
    <a href="<?php pll_e('Header-Logo');?>">
    <img class="daytour-logo" src="<?=get_bloginfo('template_directory')?>/assets/logo_180x.png"/>
    </a>
  </div>
  <div class="flex basis-40 j-a-center hide-md">
    <!-- menu here -->
    <?php include(dirname( __FILE__ ) . '/categories.php'); ?>
  </div>
  <div class="flex basis-40 j-a-center">
    <!-- menu here -->
    <?php include(dirname( __FILE__ ) . '/social-languages.php'); ?>
    <?php include(dirname( __FILE__ ) . '/hamburger-menu.php'); ?>
  </div>
</nav>
<div id="mobile-menu" class="flex MobileMenu j-a-center no-display">
  <?php include(dirname( __FILE__ ) . '/categories.php'); ?>
</div>
