<?php
$categories = get_categories( array(
  'lang'    => 'es',
  'orderby' => 'date',
  'order'   => 'ASC'
) );

?>
<nav id="categories" role="navigation">
  <ul class="flex">
    <?php 
      foreach($categories as $category):
        $term_id_category =  $category->term_id;
        $id_category = pll_get_term( $term_id_category );
        $category = get_term( $id_category );  
        $slug_category =  $category->slug;
        $icon = get_field('category_icon','category_'.$id_category);
        $name = $category->name;
    ?>
      <li class="flex"> 
        <a class="flex site-nav__label" href="/<?=$slug_category?>">
          <i class="spli-<?=$icon?> link-logo"></i> 
          <?=$name?>
        </a>
      </li>
    <?php
      endforeach;
    ?>
  </ul>
</nav>