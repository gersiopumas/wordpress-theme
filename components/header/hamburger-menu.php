<button id="burguermenu" class="hamburger hamburger--squeeze" type="button" onclick="javascript:toggleMobile()">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
</button>

<script>
  function toggleMobile(){
    let burguermenu = document.getElementById('burguermenu');
    if (burguermenu.classList.contains('is-active')) {
      burguermenu.classList.remove('is-active')
      toggleMenu();
    } else {
      burguermenu.classList.add('is-active')
      toggleMenu();
    }
  }
  function toggleMenu(){
    let navMobile = document.getElementById('mobile-menu');
    if (navMobile.classList.contains('no-display')){
      navMobile.classList.remove('no-display')
    } else {
      navMobile.classList.add('no-display')
    }
  }
    
</script>