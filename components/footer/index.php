
<footer class="flex flex-row force-flex-wrap">
  <div class="flex basis-40 j-a-center mobile-100">
    <?php include(dirname( __FILE__ ) . '/footer-categories.php'); ?>
  </div>
  <div class="flex flex-col basis-20 j-a-center mobile-100">
    <p> 
      <?php pll_e('Footer-Accepted-Card'); ?> 
    </p>
    <div class="flex flex-row j-a-center">
      <img class="max-width-100 " src="<?=get_bloginfo('template_directory')?>/assets/master_small.png"/>
      <img class="max-width-100 " src="<?=get_bloginfo('template_directory')?>/assets/visa_small.png"/>
    </div>
    
  </div>
  <div class="flex flex-col basis-20 j-a-center mobile-100">
    <p>
      <?php pll_e('Footer-Search-Us-tripadvisor'); ?> 
    </p>
    <img class="" src="<?=get_bloginfo('template_directory')?>/assets/tripfooter_small.png"/>

  </div>
  <div class="flex flex-col basis-20 j-a-center mobile-100">
    <a href="https://www.facebook.com/daytourbacalar" aria-describedby="a11y-external-message">
      <i class="spli-facebook footer-fb"></i>
    </a>
  </div>
  <div class="flex flex-row basis-100 j-a-center ">
    <a href="<?php pll_e('Footer-Privacy-URL');?>">
      <?php pll_e('Footer-Privacy');?>
    </a>
    <a href="<?php pll_e('Footer-Cancelation-URL');?>">
      <?php pll_e('Footer-Cancelation');?>
    </a>
  </div>
</footer>