
<div class="flex flex-col absolute j-a-center circle blue-bg">
  <p class="white-text bold">
    <?php pll_e('Banner-Tour-Title'); ?>
  </p>
  <p class="yellow-text bold ">$ 550.00 MXN</p>
  <a id="reserve" href="#" class="border-radius blue-border white-text red-bg reserve-hover">
    <?php pll_e('Reservation-Text'); ?>
  </a>
</div>