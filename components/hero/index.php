<div class="flex flex-row flex-wrap basis-100 j-a-center hero ">
  <div class="flex flex-col basis-30 hero-left">
    <h1 class="flex basis-100 blue-text">
      <?php pll_e('Banner-Title'); ?>
    </h1>
    <h2 class="flex basis-100 blue-text">
      <?php pll_e('Banner-Subtitle'); ?>
    </h2>
    <p>
      <?php pll_e('Banner-Experiences-Text'); ?>
    </p>
    <a class="flex flex-start border-radius yellow-bg white-text view-tour-hover" href="<?php pll_e('Banner-View-Tours-URL');?>">
      <?php pll_e('Banner-View-Tours'); ?>
    </a>
  </div>
  <div class="flex flex-col basis-70 j-a-center no-overflow">
    <?php include(dirname( __FILE__ ) . '/paddle-circle.php');?> 
    <img class="margin-l-30" src="<?=get_bloginfo("template_directory")?>/assets/fotohome.png"/>
  </div>
</div>