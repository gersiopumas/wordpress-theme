<?php
$categories = get_categories( array(
  'lang'    => 'es',
  'orderby' => 'date',
  'order'   => 'ASC'
) );
?>
<section class="flex flex-col j-a-center top-tours-container">
  <p class="flex j-a-center blue-text top-tours-title">
    <strong><?php pll_e("Section-Category-Title"); ?></strong>
  </p>
  <p>
    <?php pll_e("Section-Category-Description"); ?>
  </p>
  <div class="flex flex-row force-flex-wrap width-82 j-a-center">
    <?php
      foreach($categories as $category):
        $term_id_category =  $category->term_id;
        $id_category = pll_get_term( $term_id_category );
        $category = get_term( $id_category );  
        $slug_category =  $category->slug;
        $icon = get_field('category_icon','category_'.$id_category);
        $imgUrl = get_field('category_image','category_'.$id_category);
        
        $name = $category->name;
    ?>
    <div class="flex flex-col basis-50 category-item">
      
      <img class="max-img-250" src="<?=$imgUrl ? $imgUrl : "https://via.placeholder.com/200"?>"/>
      <a href="/<?=$slug_category?>">
      <div class="flex flex-row j-a-center dark-transparency-bg white-text">
          <i class="spli-<?=$icon?> bold category-index-icon"></i>
          <p class="flex category-index-title bold">
            <?=$name?>
          </p>
        </div>
      </a>
    </div>
    <?php endforeach; ?>
  </div>
  
</section>