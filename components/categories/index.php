<div class="flex flex-col basis-50 category-item">
  <img class="max-img-250" src="https://via.placeholder.com/1200"/>
  <div class="flex flex-row j-a-center dark-transparency-bg white-text">
    <i class="spli-velero bold category-index-icon"></i>
    <p class="flex category-index-title bold">
      <?php pll_e('Reservation-Text');?>
    </p>
  </div>
</div>