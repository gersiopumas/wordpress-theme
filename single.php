<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Daytour_bacalar
 */

get_header();
?>

	<main id="primary" class="flex site-main j-a-center">
		<div class="width-82">
			<?php
				while ( have_posts() ) :
					the_post();
					the_content(); 
				endwhile; // End of the loop.
			?>
		</div>
	</main><!-- #main -->

<?php
get_footer();