<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Daytour_bacalar
 */

get_header();
?>

	<main id="primary" class="flex flex-col site-main">
		<?php include(dirname( __FILE__ ) . '/components/hero/index.php'); ?>
    <?php include(dirname( __FILE__ ) . '/components/tours/top-tours.php'); ?>
    <?php include(dirname( __FILE__ ) . '/components/categories/top-categories.php'); ?>
    <?php include(dirname( __FILE__ ) . '/components/tripadvisor/index.php'); ?>
	</main><!-- #main -->

<?php
get_footer();
