<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Daytour_bacalar
 */

?>

  <?php include(dirname( __FILE__ ) . '/components/footer/index.php'); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
