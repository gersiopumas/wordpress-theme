<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Daytour_bacalar
 */

get_header();
?>
	<main id="primary" class="flex flex-col site-main j-a-center categories-container">
		<div class="flex flex-col basis-100 j-a-center">
    	<h1 class="text-center">
    	  <?php single_cat_title(); ?> <br/>
    		<?php pll_e('Category-Our-Options'); ?>
    	</h1>
			<h2 class="upper title blue-text">
    		<?php pll_e('Category-Enjoy-Bacalar'); ?>
			</h2>
			<div class="width-50 text-center"><?=category_description()?>
			</div>	
		</div>
		<?php include(dirname( __FILE__ ) . '/components/tours/categories-section.php'); ?>

	</main><!-- #main -->

<?php
get_footer();
