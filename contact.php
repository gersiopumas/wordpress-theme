<?php /* Template Name: Contact Page */ ?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Daytour_bacalar
 */

get_header();
?>

	<main id="primary" class="flex site-main">
    <div class="flex flex-row flex-wrap basis-100 j-a-center hero ">
      <div class="flex flex-col basis-70 j-a-center no-overflow hero-left ">
        <img class="margin-r-30" src="<?=get_bloginfo("template_directory")?>/assets/contacto.png"/>
      </div>
      <div class="flex flex-col basis-30 j-a-center">
        <div class="width-82 contacto">
        <?php
        while ( have_posts() ) :
		      the_post();
		  	  get_template_part( 'template-parts/contact', 'page' );
		    endwhile; // End of the loop.
        ?>
        </div>
      </div>
    </div>
		  

	</main><!-- #main -->

<?php
get_footer();